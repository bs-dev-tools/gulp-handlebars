# Gulp Handlebars

This process is basically a static site generator using Gulp and Handlebars.

## Getting up and running

- npm install
- gulp

### Comments or Suggestions?

Submit them [here!](https://trello.com/b/ptSYyodE/gulp-processes)
